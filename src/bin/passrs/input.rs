use rpassword;
use std::io::{stdin, stdout, Error, Write};
use text_io::read;

use crate::error::PasswordVerificationError;

pub fn get_input(prompt: &str, show: bool) -> Result<String, Error> {
    let sprompt = match prompt.split("").last().unwrap() {
        " " => prompt.strip_suffix(" ").unwrap(),
        _ => prompt,
    };
    match show {
        true => {
            print!("{} ", sprompt);
            stdout().flush()?;
            let input: String = read!();
            let _tmp = &mut String::new();
            stdin().read_line(_tmp)?;
            Ok(input)
        }
        false => rpassword::read_password_from_tty(Some(&sprompt)),
    }
}

pub fn verify(prompt: &str) -> Result<bool, Box<dyn std::error::Error>> {
    let v = get_input(prompt, true)?;
    Ok(v.eq("y") || v.eq("Y"))
}

pub fn get_password_from_user(
    echo: bool,
    path: String,
) -> Result<String, Box<dyn std::error::Error>> {
    let prompt = format!("Enter password for {}: ", path);
    let p: String = get_input(prompt.as_str(), echo)?;
    let prompt = format!("Retype password for {}: ", path);
    let v: String = get_input(prompt.as_str(), echo)?;

    if !p.eq(&v) {
        Err(Box::new(PasswordVerificationError::new()))
    } else {
        Ok(p)
    }
}
