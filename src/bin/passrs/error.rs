use passrs::colors::{Foreground, Modifier};
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct PasswordVerificationError();

impl PasswordVerificationError {
    pub fn new() -> PasswordVerificationError {
        PasswordVerificationError()
    }
}

impl fmt::Display for PasswordVerificationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: The entered passwords do not match",
            Foreground::RED,
            Modifier::RESET
        )
    }
}

impl Error for PasswordVerificationError {}
