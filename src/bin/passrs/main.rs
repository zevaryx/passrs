use passrs::colors::{Foreground, Modifier};
use passrs::{directory, error as perror, fileio, password};
use std::error::Error;
use std::path::PathBuf;

mod error;
mod help;
mod input;

use regex::Regex;
use std::env;

fn parse_command(
    program: String,
    command: String,
    args: Vec<String>,
) -> Result<(), Box<dyn Error>> {
    match command.as_str() {
        "init" => match args.len() {
            1 => match directory::init(Some(args[0].to_string())) {
                Ok(_) => Ok(()),
                Err(e) => {
                    if e.is::<perror::StorageExistsError>() {
                        let prompt = "Password store exists. Switch to this password store? (y/n)";
                        if input::verify(prompt)? {
                            fileio::write_config(&PathBuf::from(args[0].to_string()))
                        } else {
                            Err(e)
                        }
                    } else {
                        Err(e)
                    }
                }
            },
            0 => directory::init(None),
            _ => help::help(program),
        },
        "help" => help::help(program),
        "insert" => {
            let helptext = "[--echo,-e] [--force,-f] pass-name";
            let mut path: String = String::new();
            let mut force: bool = false;
            let mut echo: bool = false;
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    for arg in args {
                        match arg.as_str() {
                            "--force" | "-f" => {
                                force = true;
                            }
                            "--echo" | "-e" => {
                                echo = true;
                            }
                            _ => {
                                if path.len() == 0 && !arg.starts_with("-") {
                                    path = arg;
                                }
                            }
                        }
                    }
                    match path.len() {
                        0 => help::usage(program, command, helptext),
                        _ => {
                            let password = input::get_password_from_user(echo, path.to_owned())?;
                            password::insert(path, force, password)
                        }
                    }
                }
            }
        }
        "edit" => {
            let helptext = "[--echo,-e] pass-name";
            let mut path: String = String::new();
            let mut echo: bool = false;
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    for arg in args {
                        match arg.as_str() {
                            "--echo" | "-e" => {
                                echo = true;
                            }
                            _ => {
                                if path.len() == 0 && !arg.starts_with("-") {
                                    path = arg;
                                }
                            }
                        }
                    }
                    match path.len() {
                        0 => help::usage(program, command, helptext),
                        _ => {
                            let password = input::get_password_from_user(echo, path.to_owned())?;
                            password::edit(path, password)
                        }
                    }
                }
            }
        }
        "show" => {
            let helptext = "[show] [--clip,-c] [--qrcode,-e] pass-name";
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    let mut path: String = String::new();
                    let mut qr: bool = false;
                    let mut clip: bool = false;
                    for arg in args {
                        match arg.as_str() {
                            "--qrcode" | "-q" => {
                                qr = true;
                            }
                            "--clip" | "-c" => {
                                clip = true;
                            }
                            _ => {
                                if path.len() == 0 && !arg.starts_with("-") {
                                    path = arg;
                                }
                            }
                        }
                    }
                    match path.len() {
                        0 => help::usage(program, command, helptext),
                        _ => {
                            if let Some(result) = password::show(path, qr, clip)? {
                                println!("{}", result);
                            }
                            Ok(())
                        }
                    }
                }
            }
        }
        "find" => {
            let helptext = "find pass-names...";
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    let mut names: Vec<String> = Vec::new();
                    for arg in args {
                        names.push(arg.to_owned());
                    }
                    let result = directory::find(None, names)?;
                    println!("{}", result);
                    Ok(())
                }
            }
        }
        "rm" => {
            let helptext = "[--force,-f] [--recursive,-r] pass-name";
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    let mut path: String = String::new();
                    let mut recursive: bool = false;
                    let mut force: bool = false;
                    for arg in args {
                        match arg.as_str() {
                            "--force" | "-f" => {
                                force = true;
                            }
                            "--recursive" | "-r" => {
                                recursive = true;
                            }
                            _ => {
                                if path.len() == 0 && !arg.starts_with("-") {
                                    path = arg;
                                }
                            }
                        }
                    }
                    match path.len() {
                        0 => help::usage(program, command, helptext),
                        _ => {
                            if !force {
                                let prompt = format!("Remove path {}? (y/n) ", path);
                                let result = input::verify(&prompt)?;
                                if !result {
                                    Ok(())
                                } else {
                                    password::rm(path, recursive)
                                }
                            } else {
                                password::rm(path, recursive)
                            }
                        }
                    }
                }
            }
        }
        "mv" | "cp" => {
            let helptext = "[--force, -f] old-path new-path";
            match args.len() {
                0 => help::usage(program, command, helptext),
                _ => {
                    let remove = command.eq("mv");
                    let mut src: String = String::new();
                    let mut dest: String = String::new();
                    let mut force: bool = false;
                    for arg in args {
                        match arg.as_str() {
                            "--force" | "-f" => {
                                force = true;
                            }
                            _ => {
                                if src.len() == 0 && !arg.starts_with("-") {
                                    src = arg;
                                } else if dest.len() == 0 && !arg.starts_with("-") {
                                    dest = arg;
                                }
                            }
                        }
                    }
                    if src.len() == 0 || dest.len() == 0 {
                        help::usage(program, command, helptext)
                    } else {
                        password::mv(src, dest, force, remove)
                    }
                }
            }
        }
        "generate" | "gen" => {
            let _helptext =
                "[--no-symbols,-n] [--qrcode,-q] [--clip,-c] [--force,-f] [pass-name] [length]";
            match args.len() {
                0 => {
                    if let Some(p) = password::generate(None, true, false, false, false, 32)? {
                        println!(
                            "Your new password is:\n{}{}{}",
                            Foreground::YELLOW,
                            p,
                            Modifier::RESET
                        );
                    }
                    Ok(())
                }
                _ => {
                    let check_num = Regex::new(r"^[0-9]{1,}$")?;
                    let mut path: String = String::new();
                    let mut symbols: bool = true;
                    let mut clip: bool = false;
                    let mut qr: bool = false;
                    let mut force: bool = false;
                    let mut length: usize = 32;
                    for arg in args {
                        match arg.as_str() {
                            "--no-symbols" | "-n" => {
                                symbols = false;
                            }
                            "--qrcode" | "-q" => {
                                qr = true;
                            }
                            "--clip" | "-c" => {
                                clip = true;
                            }
                            "--force" | "-f" => {
                                force = true;
                            }
                            _ => {
                                if check_num.is_match(&arg) {
                                    length = arg.parse::<usize>()?;
                                    if length < 1 {
                                        length = 32;
                                    }
                                } else if path.len() == 0 && !arg.starts_with("-") {
                                    path = arg;
                                }
                            }
                        }
                    }
                    let result = match path.len() {
                        0 => password::generate(None, symbols, clip, qr, force, length)?,
                        _ => password::generate(Some(path), symbols, clip, qr, force, length)?,
                    };
                    if let Some(p) = result {
                        match qr {
                            true => println!("{}", p),
                            false => println!(
                                "Your new password is:\n{}{}{}",
                                Foreground::YELLOW,
                                p,
                                Modifier::RESET
                            ),
                        }
                    }
                    Ok(())
                }
            }
        }
        "ls" => match args.len() {
            0 => {
                let tree = directory::show_tree(None, None)?;
                println!("{}", tree);
                Ok(())
            }
            _ => {
                let subdirectory = args[0].to_string();
                let tree = directory::show_tree(Some(subdirectory.to_owned()), None)?;
                println!("{}", tree);
                fileio::write_config(&PathBuf::from(subdirectory))
            }
        },
        "version" => help::version(),
        _ => match args.len() {
            0 => {
                if let Some(p) = password::show(command, false, false)? {
                    println!("{}", p);
                }
                Ok(())
            }
            _ => {
                let helptext = "[show] [--clip,-c] [--qrcode,-e] pass-name";
                let mut path: String = String::new();
                let mut qr: bool = false;
                let mut clip: bool = false;
                for arg in args {
                    match arg.as_str() {
                        "--qrcode" | "-q" => {
                            qr = true;
                        }
                        "--clip" | "-c" => {
                            clip = true;
                        }
                        _ => {
                            if path.len() == 0 && !arg.starts_with("-") {
                                path = arg;
                            }
                        }
                    }
                }
                match path.len() {
                    0 => help::usage(program, command, helptext),
                    _ => {
                        let result = password::show(path, qr, clip)?;
                        if let Some(p) = result {
                            println!("{}", p);
                        }
                        Ok(())
                    }
                }
            }
        },
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut args: Vec<String> = env::args().collect();
    let program: String = args.remove(0).to_owned();

    if args.len() == 0 {
        let result = directory::show_tree(None, None)?;
        println!("{}", result);
        Ok(())
    } else {
        let command = args.remove(0).to_owned();
        match parse_command(program, command, args) {
            Ok(_) => Ok(()),
            Err(e) => {
                println!("{}", e);
                Ok(())
            }
        }
    }
}
