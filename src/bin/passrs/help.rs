fn fix_program(program: String) -> String {
    let sep = match cfg!(windows) {
        true => "\\",
        false => "/",
    };

    let tmp: String = match program.split(sep).collect::<Vec<&str>>().last() {
        Some(v) => v.to_string(),
        None => program,
    };

    match cfg!(windows) {
        true => tmp.replace(".exe", ""),
        false => tmp,
    }
}

pub fn help(program: String) -> Result<(), Box<dyn std::error::Error>> {
    let sprogram = fix_program(program);

    println!(
        "
Usage:
    {0} init [subfolder]
        Initialize a new password store with a new key.
    {0} [ls] [subfolder]
        List passwords.
    {0} find pass-names...
        List passwords that match pass-names.
    {0} [show] [--qrcode,-q] [--clip,-c] pass-name
        Show existing password. Optionally, show password as a QR code.
        Optionally, copy password to clipboard.
    {0} insert [--echo,-e] [--force,-f] pass-name
        Insert new password. Optionally, echo the password back to the console
        during entry. Prompt before overwriting existing password unless forced.
    {0} edit pass-name [--echo,-e]
        Insert a new password or edit an existing password.
    {0} generate [--echo,-e] [--no-symbols,-n] [--force,-f] pass-name [pass-length]
        Generate a new password of pass-length (or 32 if unspecified) with optionally no symbols.
        Prompt before overwriting existing password unless forced.
        Optionally, echo the password back to the console.
    {0} rm [--recursive,-r] [--force,-f] pass-name
        Remove existing password or directory, optionally forcefully
    {0} mv [--force,-f] old-path new-path
        Renames or moves old-path to new-path, optionally forcefully, reencrypting as it goes.
    {0} cp [--force,-f] old-path new-path
        Copies old-path to new-path, optionally forcefully, reencrypting as it goes.
    {0} git git-command-args...
        If the password store is a git repository, execute a git command
        specified by git-command-args.
    {0} help
        Show this help text
    {0} version
        Show version information.
    ",
        sprogram
    );

    Ok(())
}

pub fn version() -> Result<(), Box<dyn std::error::Error>> {
    println!(
        "
┏-----------------------------------------┓
|  passrs: A Rust implementation of pass  |
|                                         |
|                v{}                   |
|             Casey Burklow               |
|           zevaryx@gmail.com             |
┗-----------------------------------------┛
    ",
        env!("CARGO_PKG_VERSION")
    );
    Ok(())
}

pub fn usage(
    program: String,
    command: String,
    text: &str,
) -> Result<(), Box<dyn std::error::Error>> {
    let sprogram = fix_program(program);

    println!("Usage: {} {} {}", sprogram, command, text);
    Ok(())
}
