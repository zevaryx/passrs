use cli_clipboard::set_contents;
use qrcode::{render::unicode, QrCode};
use std::error::Error;

use crate::colors::{Foreground, Modifier};
use crate::error;
use crate::fileio;
use crate::keys;

/// Show a password
///
/// # Arguments
///
/// * `path` - Path to password
/// * `qr` - Display a QR code instead of plaintext password
/// * `clip` - Copy password to clipboard
pub fn show(path: String, qr: bool, clip: bool) -> Result<Option<String>, Box<dyn Error>> {
    let (_, filepath) = fileio::parse_path(&path)?;

    if !filepath.exists() {
        Err(Box::new(error::NotFoundError::new(path)))
    } else {
        match keys::decrypt_file(&filepath) {
            Ok(p) => {
                if clip {
                    set_contents(p.to_owned())?;
                    Ok(None)
                } else if qr {
                    let code = QrCode::new(p.as_bytes())?;

                    let output = code
                        .render::<unicode::Dense1x2>()
                        .dark_color(unicode::Dense1x2::Light)
                        .light_color(unicode::Dense1x2::Dark)
                        .build();
                    Ok(Some(output))
                } else {
                    Ok(Some(p))
                }
            }
            Err(e) => Err(e),
        }
    }
}

/// Insert new password
///
/// # Arguments
///
/// * `path` - Path to password
/// * `force` - Force insert the password
/// * `password` - Password to write
pub fn insert(path: String, force: bool, password: String) -> Result<(), Box<dyn Error>> {
    let (folder, filepath) = fileio::parse_path(&path)?;

    if filepath.exists() && !force {
        Err(Box::new(error::PasswordExistsError::new(path, false)))
    } else {
        fileio::init_new_path(&folder)?;
        let nonce = keys::generate_nonce();
        let key = match keys::get_key(&folder.join(".key")) {
            Ok(v) => v,
            Err(_) => keys::write_key(&folder.join(".key"), None)?,
        };

        let ciphertext = keys::encrypt(&password.as_str(), Some(key), Some(nonce))?;
        keys::write_cipher(ciphertext, nonce, &filepath)
    }
}

/// Generate a new password
///
/// # Arguments
///
/// * `path` - Optional path to save to, None to print
/// * `symbols` - Whether or not to use symbols
/// * `clip` - Copy to clipboard if not saving to path
/// * `qr` - Display a QR code if not saving to path
/// * `force` - Force insert password
/// * `length` - Length of the password to generate
pub fn generate(
    path: Option<String>,
    symbols: bool,
    clip: bool,
    qr: bool,
    force: bool,
    length: usize,
) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let generator = passwords::PasswordGenerator::new();
    let password: String = generator
        .length(length)
        .lowercase_letters(true)
        .uppercase_letters(true)
        .numbers(true)
        .symbols(symbols)
        .generate_one()?;

    match path {
        Some(v) => {
            insert(v, force, password)?;
            Ok(None)
        }
        None => {
            if clip {
                set_contents(password.to_owned())?;
                Ok(None)
            } else if qr {
                let code = QrCode::new(password.as_bytes())?;

                let output = code
                    .render::<unicode::Dense1x2>()
                    .dark_color(unicode::Dense1x2::Light)
                    .light_color(unicode::Dense1x2::Dark)
                    .build();
                Ok(Some(output))
            } else {
                Ok(Some(password))
            }
        }
    }
}

/// Remove a password
///
/// # Arguments
///
/// * `path` - Path to password
/// * `recursive` - Whether or not to delete folder
pub fn rm(path: String, recursive: bool) -> Result<(), Box<dyn std::error::Error>> {
    let (_, filepath) = fileio::parse_path(&path)?;
    let t: &str = match filepath.is_dir() {
        true => "folder",
        false => "file",
    };
    let t_color: Foreground = match filepath.is_dir() {
        true => Foreground::CYAN,
        false => Foreground::YELLOW,
    };
    if !filepath.exists() {
        Err(Box::new(error::NotFoundError::new(path)))
    } else if filepath.is_dir() && !recursive {
        Err(Box::new(error::IsDirectoryError::new(path)))
    } else {
        match fileio::rm(&filepath) {
            Ok(_) => Ok(()),
            Err(_) => {
                let message = format!(
                    "Failed to remove {} {}{}{}",
                    t,
                    t_color,
                    path,
                    Modifier::RESET
                );
                Err(Box::new(error::FileError::new(message)))
            }
        }
    }
}

/// Edit a password
///
/// # Arguments
///
/// * `path` - Path to password
/// * `password` - New password
pub fn edit(path: String, password: String) -> Result<(), Box<dyn Error>> {
    let (folder, filepath) = fileio::parse_path(&path)?;

    if !filepath.exists() {
        Err(Box::new(error::NotFoundError::new(path)))
    } else {
        let nonce = keys::generate_nonce();
        let key = match keys::get_key(&folder.join(".key")) {
            Ok(v) => v,
            Err(_) => {
                let k = keys::generate_key();
                keys::write_key(&folder.join(".key"), Some(k.to_vec()))?;
                k
            }
        };

        let cipher = keys::encrypt(&password.as_str(), Some(key), Some(nonce))?;
        keys::write_cipher(cipher, nonce, &filepath)
    }
}

/// Move a password
///
/// # Arguments
///
/// * `src` - Source path
/// * `dest` - Destination path
/// * `force` - Force override
/// * `remove` - Remove origin (true to move, false to copy)
pub fn mv(src: String, dest: String, force: bool, remove: bool) -> Result<(), Box<dyn Error>> {
    let (_, src_filepath) = fileio::parse_path(&src)?;
    let (_, dest_filepath) = fileio::parse_path(&dest)?;

    if dest_filepath.exists() && !force {
        Err(Box::new(error::PasswordExistsError::new(dest, true)))
    } else {
        fileio::mv(&src_filepath, &dest_filepath, remove)
    }
}
