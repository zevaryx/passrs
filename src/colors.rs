use std::fmt;

/// Terminal prefix
static PREFIX: &'static str = "\u{001B}[0;";

/// Terminal modifiers
#[allow(dead_code)]
pub enum Modifier {
    RESET,
    BRIGHT,
    DIM,
    UNDERSCORE,
    BLINK,
    REVERSE,
    HIDDEN,
}

impl Modifier {
    /// Returns the modifier as a string
    pub fn as_string(&self) -> String {
        let suffix = match self {
            &Modifier::RESET => "0m",
            &Modifier::BRIGHT => "1m",
            &Modifier::DIM => "2m",
            &Modifier::UNDERSCORE => "4m",
            &Modifier::BLINK => "5m",
            &Modifier::REVERSE => "7m",
            &Modifier::HIDDEN => "8m",
        };
        PREFIX.to_string() + suffix
    }
}

impl fmt::Display for Modifier {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_string())
    }
}

/// Terminal foreground colors
#[allow(dead_code)]
pub enum Foreground {
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
}

impl Foreground {
    /// Returns the foreground color as a string
    pub fn as_string(&self) -> String {
        let suffix = match self {
            &Foreground::BLACK => "30m",
            &Foreground::RED => "31m",
            &Foreground::GREEN => "32m",
            &Foreground::YELLOW => "33m",
            &Foreground::BLUE => "34m",
            &Foreground::MAGENTA => "35m",
            &Foreground::CYAN => "36m",
            &Foreground::WHITE => "37m",
        };
        PREFIX.to_string() + suffix
    }
}

impl fmt::Display for Foreground {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_string())
    }
}

/// Terminal background colors
#[allow(dead_code)]
pub enum Background {
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    MAGENTA,
    CYAN,
    WHITE,
}

impl Background {
    /// Returns the foreground color as a string
    pub fn as_string(&self) -> String {
        let suffix = match self {
            &Background::BLACK => "40m",
            &Background::RED => "41m",
            &Background::GREEN => "42m",
            &Background::YELLOW => "43m",
            &Background::BLUE => "44m",
            &Background::MAGENTA => "45m",
            &Background::CYAN => "46m",
            &Background::WHITE => "47m",
        };
        PREFIX.to_string() + suffix
    }
}

impl fmt::Display for Background {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.as_string())
    }
}
