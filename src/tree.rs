use std::error::Error;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};

use crate::colors::{Foreground, Modifier};

fn visit_dirs(
    dir: &Path,
    depth: usize,
    level: usize,
    prefix: String,
    colorize: bool,
    show_all: bool,
    names: &Option<Vec<String>>,
) -> Result<Option<String>, Box<dyn std::error::Error>> {
    let mut result: String = String::new();
    if (level != 0) & (depth == level) {
        return Ok(None);
    }

    if dir.is_dir() {
        let entry_set = fs::read_dir(dir)?; // contains DirEntry
        let mut entries = entry_set
            .filter_map(|v| match v.ok() {
                Some(v) => {
                    if show_all {
                        return Some(v);
                    } else {
                        if v.file_name().to_str()?.starts_with(".") {
                            return None;
                        } else {
                            Some(v)
                        }
                    }
                }
                None => None,
            })
            .collect::<Vec<_>>();
        entries.sort_by(|a, b| a.path().file_name().cmp(&b.path().file_name()));

        for (index, entry) in entries.iter().enumerate() {
            let path = entry.path();
            let filename = entry.file_name();

            let mut p = true;
            if let Some(v) = names {
                if path.is_file()
                    && !v
                        .into_iter()
                        .any(|x| filename.to_string_lossy().contains(x))
                {
                    p = false;
                } else {
                    p = true;
                }
            }

            // TODO: Cleanup display when using find
            if index == entries.len() - 1 {
                if p {
                    let more = format!("{}└── {}", prefix, color_output(colorize, &path)?);
                    result.push_str(&more);
                }
                if path.is_dir() {
                    let depth = depth + 1;
                    let prefix_new = prefix.clone() + "    ";
                    if let Some(more) =
                        visit_dirs(&path, depth, level, prefix_new, colorize, show_all, names)?
                    {
                        result.push_str(&more);
                    }
                }
            } else {
                if p {
                    let more = format!("{}├── {}", prefix, color_output(colorize, &path)?);
                    result.push_str(&more);
                }
                if path.is_dir() {
                    let depth = depth + 1;
                    let prefix_new = prefix.clone() + "│   ";
                    if let Some(more) =
                        visit_dirs(&path, depth, level, prefix_new, colorize, show_all, names)?
                    {
                        result.push_str(&more);
                    }
                }
            }
        }
    }
    Ok(Some(result))
}

fn color_output(colorize: bool, path: &Path) -> io::Result<String> {
    let filename = path.file_name().unwrap().to_str().unwrap();
    let symlink = match fs::read_link(path) {
        Ok(v) => v,
        Err(_err) => PathBuf::new(),
    };

    let print_name;
    if !symlink.to_str().unwrap().is_empty() {
        print_name = format!("{} -> {}", filename, symlink.to_str().unwrap());
    } else {
        print_name = filename.to_string();
    }

    match colorize {
        true => {
            if path.is_dir() {
                Ok(format!(
                    "{}{}{}",
                    Foreground::CYAN,
                    print_name,
                    Modifier::RESET
                ))
            } else {
                Ok(format!(
                    "{}{}{}",
                    Foreground::YELLOW,
                    print_name,
                    Modifier::RESET
                ))
            }
        }
        false => Ok(format!("{}", print_name)),
    }
}

pub fn run(
    show_all: bool,
    colorize: bool,
    level: usize,
    dir: &Path,
    name: Option<Vec<String>>,
) -> Result<String, Box<dyn Error>> {
    let result = visit_dirs(
        &dir,
        0,
        level,
        String::from("\n"),
        colorize,
        show_all,
        &name,
    )?;
    match result {
        Some(v) => Ok(v),
        None => Ok("".to_string()),
    }
}
