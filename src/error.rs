use std::error::Error;
use std::fmt;

use crate::colors::{Foreground, Modifier};

/// Folder isn't valid for password storage
#[derive(Debug)]
pub struct InvalidStorageError(String);

impl InvalidStorageError {
    pub fn new(message: String) -> InvalidStorageError {
        InvalidStorageError(message)
    }
}

impl fmt::Display for InvalidStorageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Please initialize password storage at {}{}{}",
            Foreground::RED,
            Modifier::RESET,
            Foreground::GREEN,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for InvalidStorageError {}

/// Folder is not a password storage
#[derive(Debug)]
pub struct NonStorageError(String);

impl NonStorageError {
    pub fn new(message: String) -> NonStorageError {
        NonStorageError(message)
    }
}

impl fmt::Display for NonStorageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Non-storage folder exists at {}{}{}",
            Foreground::RED,
            Modifier::RESET,
            Foreground::GREEN,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for NonStorageError {}

/// Generic decryption error
#[derive(Debug)]
pub struct DecryptionError(String, String);

impl DecryptionError {
    pub fn new(path: String, message: String) -> DecryptionError {
        DecryptionError(path, message)
    }
}

impl fmt::Display for DecryptionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Failed to decrypt password {}{}{}: {}",
            Foreground::RED,
            Modifier::RESET,
            Foreground::YELLOW,
            self.0,
            Modifier::RESET,
            self.1
        )
    }
}

impl Error for DecryptionError {}

/// Generic encryption error
#[derive(Debug)]
pub struct EncryptionError(String);

impl EncryptionError {
    pub fn new(message: String) -> EncryptionError {
        EncryptionError(message)
    }
}

impl fmt::Display for EncryptionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Failed to encrypt plaintext: {}",
            Foreground::RED,
            Modifier::RESET,
            self.0
        )
    }
}

impl Error for EncryptionError {}

/// Password not found
#[derive(Debug)]
pub struct NotFoundError(String);

impl NotFoundError {
    pub fn new(message: String) -> NotFoundError {
        NotFoundError(message)
    }
}

impl fmt::Display for NotFoundError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Password {}{}{} not in storage",
            Foreground::RED,
            Modifier::RESET,
            Foreground::YELLOW,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for NotFoundError {}

/// Password to remove is a directory, not a password
#[derive(Debug)]
pub struct IsDirectoryError(String);

impl IsDirectoryError {
    pub fn new(message: String) -> IsDirectoryError {
        IsDirectoryError(message)
    }
}

impl fmt::Display for IsDirectoryError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: {}{}{} is a directory. Please re-run with [--recursive,-r] to remove directory.",
            Foreground::RED,
            Modifier::RESET,
            Foreground::CYAN,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for IsDirectoryError {}

/// Generic file error
#[derive(Debug)]
pub struct FileError(String);

impl FileError {
    pub fn new(message: String) -> FileError {
        FileError(message)
    }
}

impl fmt::Display for FileError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Password {}{}{} not in storage",
            Foreground::RED,
            Modifier::RESET,
            Foreground::YELLOW,
            Modifier::RESET,
            self.0
        )
    }
}

impl Error for FileError {}

/// Duplicate password error
#[derive(Debug)]
pub struct PasswordExistsError(String, bool);

impl PasswordExistsError {
    pub fn new(message: String, dir: bool) -> PasswordExistsError {
        PasswordExistsError(message, dir)
    }
}

impl fmt::Display for PasswordExistsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let c = match self.1 {
            true => Foreground::CYAN,
            false => Foreground::YELLOW,
        };
        write!(
            f,
            "{}Error{}: Password {}{}{} already in storage. Please re-run with [-f, --force] to override this password",
            Foreground::RED,
            Modifier::RESET,
            c,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for PasswordExistsError {}

/// Storage already exists
#[derive(Debug)]
pub struct StorageExistsError(String);

impl StorageExistsError {
    pub fn new(message: String) -> StorageExistsError {
        StorageExistsError(message)
    }
}

impl fmt::Display for StorageExistsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}Error{}: Storage exists at {}{}{}",
            Foreground::RED,
            Modifier::RESET,
            Foreground::CYAN,
            self.0,
            Modifier::RESET
        )
    }
}

impl Error for StorageExistsError {}
