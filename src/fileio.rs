use dirs::home_dir;
use std::error::Error;
use std::fs::{self, File};
use std::io;
use std::io::prelude::*;
use std::path::{Path, PathBuf};

use crate::error;
use crate::keys;

/// Get passrs config from $HOME/.passrs
fn get_config() -> Result<Option<String>, Box<dyn Error>> {
    let config_path = match home_dir() {
        Some(v) => v.join(".passrs"),
        None => Path::new(".passrs").to_owned(),
    };

    if !config_path.exists() {
        Ok(None)
    } else {
        let mut contents = String::new();
        let mut file = File::open(config_path)?;
        file.read_to_string(&mut contents)?;
        Ok(Some(contents))
    }
}

/// Write config to path
///
/// # Arguments
///
/// * `path` - Path to write config to
pub fn write_config(path: &PathBuf) -> Result<(), Box<dyn Error>> {
    let config_path = match home_dir() {
        Some(v) => v.join(".passrs"),
        None => Path::new(".passrs").to_owned(),
    };
    if config_path.exists() {
        match get_config()? {
            Some(v) => match v.eq(&path.to_string_lossy()) {
                true => Ok(()),
                false => {
                    let mut file = File::create(config_path)?;
                    Ok(file.write_all(path.to_string_lossy().as_bytes())?)
                }
            },
            None => {
                let mut file = File::create(config_path)?;
                Ok(file.write_all(path.to_string_lossy().as_bytes())?)
            }
        }
    } else {
        let mut file = File::create(config_path)?;
        Ok(file.write_all(path.to_string_lossy().as_bytes())?)
    }
}

/// Get password storage
///
/// # Arguments
///
/// * `dir` - Optional path to storage, default is `$HOME/.passrs_storage`
pub fn get_directory(dir: Option<String>) -> Result<PathBuf, Box<dyn Error>> {
    match dir {
        Some(v) => Ok(Path::new(&v).to_owned()),
        None => match get_config()? {
            Some(c) => Ok(Path::new(&c).to_owned()),
            None => {
                let path = match home_dir() {
                    Some(v) => v.join(".passrs_storage"),
                    None => Path::new(".passrs_storage").to_owned(),
                };
                write_config(&path)?;
                Ok(path)
            }
        },
    }
}

/// Parse a path and return the file and parent folders
///
/// # Arguments
///
/// * `path` - Password path
pub fn parse_path(path: &String) -> Result<(PathBuf, PathBuf), Box<dyn Error>> {
    let mut folder: PathBuf = get_directory(None)?;
    if !folder.exists() {
        Err(Box::new(error::InvalidStorageError::new(
            folder.to_string_lossy().into(),
        )))
    } else {
        let mut path_parts: Vec<&str> = path.split("/").collect();
        let filename = match path_parts.pop() {
            Some(v) => v,
            None => "",
        };
        if path_parts.len() > 0 {
            for part in path_parts {
                folder = folder.join(part);
            }
        }
        let filepath = folder.join(filename);
        Ok((folder, filepath))
    }
}

/// Create a new password storage subdirectory
///
/// # Arguments
///
/// * `path` - Path to subdirectory
pub fn init_new_path(path: &PathBuf) -> Result<(), Box<dyn Error>> {
    if !path.exists() {
        fs::create_dir_all(&path)?;
        let mut parent = path.parent().unwrap();
        let mut parent_key = parent.join(".key");
        while !parent_key.exists() {
            keys::write_key(&parent_key, None)?;
            parent = parent.parent().unwrap();
            parent_key = parent.join(".key");
        }
    }
    Ok(())
}

/// Remove a password path
///
/// # Arguments
///
/// * `path` - Path to remove
pub fn rm(path: &PathBuf) -> Result<(), io::Error> {
    match path.is_dir() {
        true => fs::remove_dir_all(path),
        false => fs::remove_file(path),
    }
}

/// Move a file
///
/// # Arguments
///
/// * `src` - Source path
/// * `dest` - Destination path
/// * `remove` - If we remove the source (true for move, false for copy)
fn mv_file(src: &PathBuf, dest: &PathBuf, remove: bool) -> Result<(), Box<dyn Error>> {
    let dest_parent = match dest.parent() {
        Some(v) => v,
        None => dest,
    };
    if !dest_parent.exists() {
        fs::create_dir_all(dest_parent)?;
    }
    if dest_parent.is_dir() {
        init_new_path(&dest_parent.to_owned())?;
        let password = keys::decrypt_file(&src)?;
        let nonce = keys::generate_nonce();
        let key = match keys::get_key(&dest_parent.join(".key")) {
            Ok(v) => v,
            Err(_) => keys::write_key(&dest_parent.join(".key"), None)?,
        };
        let cipher = keys::encrypt(&password.as_str(), Some(key), Some(nonce)).unwrap();
        keys::write_cipher(cipher, nonce, &dest)?;
        if remove {
            rm(&src)?;
        }
        Ok(())
    } else {
        Err(Box::new(error::FileError::new(
            "Destination parent not a folder".to_string(),
        )))
    }
}

/// Move a folder
///
/// # Arguments
///
/// * `src` - Source path
/// * `dest` - Destination path
/// * `remove` - If we remove the source (true for move, false for copy)
fn mv_folder(src: &PathBuf, dest: &PathBuf, remove: bool) -> Result<(), Box<dyn Error>> {
    init_new_path(&dest)?;
    for entry in fs::read_dir(&src)? {
        let entry = entry?;
        let ty = entry.file_type()?;
        if ty.is_dir() {
            mv_folder(&entry.path(), &dest.join(entry.file_name()), remove)?;
        } else {
            if entry.file_name() != ".key" {
                mv_file(&entry.path(), &dest.join(entry.file_name()), remove)?;
            }
        }
    }
    if remove {
        rm(&src)?;
    }
    Ok(())
}

/// Move a file/folder
///
/// # Arguments
///
/// * `src` - Source path
/// * `dest` - Destination path
/// * `remove` - If we remove the source (true for move, false for copy)
pub fn mv(src: &PathBuf, dest: &PathBuf, remove: bool) -> Result<(), Box<dyn Error>> {
    if src.is_file() {
        mv_file(src, dest, remove)
    } else {
        mv_folder(src, dest, remove)
    }
}
