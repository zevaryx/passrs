use std::{fs, path::PathBuf};

use crate::colors::{Foreground, Modifier};
use crate::error;
use crate::fileio;
use crate::keys;
use crate::tree;

/// Show password tree
///
/// # Arguments
///
/// * `subfolder` - Optional password storage to show
/// * `names` - Optional list of names to limit search to
pub fn show_tree(
    subfolder: Option<String>,
    names: Option<Vec<String>>,
) -> Result<String, Box<dyn std::error::Error>> {
    let directory = fileio::get_directory(subfolder)?;
    if !directory.join(".key").exists() {
        Err(Box::new(error::InvalidStorageError::new(
            directory.to_string_lossy().to_string(),
        )))
    } else {
        let mut result = format!(
            "Password Store [{}{}{}]\n",
            Foreground::GREEN,
            directory.to_string_lossy(),
            Modifier::RESET
        );
        match tree::run(false, true, 0, &directory, names) {
            Ok(v) => {
                result.push_str(&v.trim_start());
                Ok(result)
            }
            Err(e) => Err(e),
        }
    }
}

/// Initialize a new password storage
///
/// # Arguments
///
/// * `subfolder` - Optional subfolder to create the storage in
pub fn init(subfolder: Option<String>) -> Result<(), Box<dyn std::error::Error>> {
    let folder: PathBuf = fileio::get_directory(subfolder)?;

    if !folder.exists() {
        fs::create_dir_all(&folder)?;
        let rkey = folder.join(".key");
        keys::write_key(&rkey, None)?;
        Ok(())
    } else if folder.join(".key").exists() {
        Err(Box::new(error::StorageExistsError::new(
            folder.to_string_lossy().to_string(),
        )))
    } else {
        Err(Box::new(error::NonStorageError::new(
            folder.to_string_lossy().to_string(),
        )))
    }
}

/// Find specific names in the password storage
///
/// # Arguments
///
/// * `subfolder` - Optional password storage to search
/// * `names` - List of password names to search for
pub fn find(
    subfolder: Option<String>,
    names: Vec<String>,
) -> Result<String, Box<dyn std::error::Error>> {
    let folder: PathBuf = fileio::get_directory(subfolder.to_owned())?;

    if !folder.exists() {
        Err(Box::new(error::InvalidStorageError::new(
            folder.to_string_lossy().into(),
        )))
    } else {
        // TODO: walk directory and find all matching names
        show_tree(subfolder, Some(names))
    }
}
