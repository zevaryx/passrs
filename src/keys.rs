use base64;
use chacha20poly1305::aead::{Aead, NewAead};
use chacha20poly1305::{Key, XChaCha20Poly1305, XNonce};
use rand::Rng;
use std::fs;
use std::io::prelude::*;
use std::path::PathBuf;

use crate::error::{DecryptionError, EncryptionError};

/// Load a key
///
/// # Arguments
///
/// * `path` - Path to key file
pub fn get_key(path: &PathBuf) -> Result<Key, Box<dyn std::error::Error>> {
    let mut key_file = fs::File::open(&path)?;
    let mut buf: Vec<u8> = Vec::with_capacity(32);
    key_file.read_to_end(&mut buf)?;
    buf.reverse();
    Ok(Key::from_iter(buf))
}

/// Write a key
///
/// # Arguments
///
/// * `path` - Path to key file
/// * `key` - Optional key, None to generate new key
pub fn write_key(path: &PathBuf, key: Option<Vec<u8>>) -> Result<Key, Box<dyn std::error::Error>> {
    let mut nkey = match key {
        Some(v) => v,
        None => {
            let mut k = generate_key().to_vec();
            k.reverse();
            k
        }
    };
    let mut key_file = fs::File::create(&path)?;
    key_file.write_all(&nkey)?;
    nkey.reverse();
    Ok(Key::from_iter(nkey))
}

/// Generate a new key
pub fn generate_key() -> Key {
    let random_bytes = rand::thread_rng().gen::<[u8; 32]>();
    let key = Key::from_iter(random_bytes);

    key
}

/// Generate a new XNonce
pub fn generate_nonce() -> XNonce {
    let random_bytes = rand::thread_rng().gen::<[u8; 24]>();
    let nonce = XNonce::from_iter(random_bytes);

    nonce
}

/// Encrypt plaintext
///
/// # Arguments
///
/// * `plaintext` - plaintext to encrypt
/// * `key` - Optional key to use, None to generate new key
/// * `nonce` - Optional nonce to use, None to generate new nonce
pub fn encrypt(
    plaintext: &str,
    key: Option<Key>,
    nonce: Option<XNonce>,
) -> Result<Vec<u8>, Box<dyn std::error::Error>> {
    let key: Key = match key {
        Some(v) => v,
        None => generate_key(),
    };
    let nonce: XNonce = match nonce {
        Some(v) => v,
        None => generate_nonce(),
    };

    let cipher = XChaCha20Poly1305::new(&key);

    match cipher.encrypt(&nonce, plaintext.as_bytes().as_ref()) {
        Ok(c) => Ok(c),
        Err(e) => {
            let reason = format!("{}", e);
            Err(Box::new(EncryptionError::new(reason)))
        }
    }
}

/// Write a ciphertext to file
///
/// # Arguments
///
/// * `ciphertext` - ciphertext to write
/// * `nonce` - Nonce to write
/// * `path` - Path to write to
pub fn write_cipher(
    ciphertext: Vec<u8>,
    nonce: XNonce,
    path: &PathBuf,
) -> Result<(), Box<dyn std::error::Error>> {
    let bnonce = base64::encode(nonce);
    let bcipher = base64::encode(ciphertext);
    let mut cipherfile = fs::File::create(&path)?;
    let filetext = format!("{}\n{}", bnonce, bcipher);
    cipherfile.write_all(filetext.as_bytes())?;
    Ok(())
}

/// Decrypt plaintext
///
/// # Arguments
///
/// * `ciphertext` - ciphertext to decrypt
/// * `key` - Key to use
/// * `nonce` - Nonce to use
/// * `path` - Path to encrypted file for errors
pub fn decrypt(
    ciphertext: Vec<u8>,
    key: Key,
    nonce: XNonce,
    path: &PathBuf,
) -> Result<String, Box<dyn std::error::Error>> {
    let cipher = XChaCha20Poly1305::new(&key);
    match cipher.decrypt(&nonce, ciphertext.as_ref()) {
        Ok(v) => {
            let s = std::str::from_utf8(&v)?;
            Ok(s.to_string())
        }
        Err(e) => {
            let reason = format!("{}", e);
            Err(Box::new(DecryptionError::new(
                path.to_string_lossy().to_string(),
                reason,
            )))
        }
    }
}

/// Decrypt a file
///
/// # Arguments
///
/// * `path` - Path to read
pub fn decrypt_file(path: &PathBuf) -> Result<String, Box<dyn std::error::Error>> {
    let keypath = match path.parent() {
        Some(v) => v.join(".key"),
        None => path.join(".key"),
    };
    let key = get_key(&keypath)?;

    let mut cipherfile = fs::File::open(path)?;
    let contents = &mut String::new();
    cipherfile.read_to_string(contents)?;
    let parts: Vec<&str> = contents.split("\n").collect();
    let bnonce: &str = parts[0];
    let bcipher: &str = parts[1];

    let nonce = XNonce::from_iter(base64::decode(bnonce)?);
    let ciphertext = base64::decode(bcipher)?;
    decrypt(ciphertext, key, nonce, path)
}
