# `passrs`

A Rust implementation of zx2c4's `pass` password manager.

## How it works

This is written to solve the issue of *easy* sharing of passwords with non-technical users.

It stores `XChaCha20Poly1305` keys per directory, and stores the `XNonce` on line 1 of the password file, and the encrypted password on line 2.

`passrs` aims to be as close to compatible with `pass`, with few exceptions.

## Usage

```
passrs init [subfolder]
    Initialize a new password store with a new key.
passrs [ls] [subfolder]
    List passwords.
passrs find pass-names...
    List passwords that match pass-names.
passrs [show] [--qrcode,-q] [--clip,-c] pass-name
    Show existing password. Optionally, show password as a QR code.
    Optionally, copy password to clipboard.
passrs grep [GREPOPTIONS] search-string
    Search for password files containing search-string when decrypted.
passrs insert [--echo,-e] [--force,-f] pass-name
    Insert new password. Optionally, echo the password back to the console
    during entry. Prompt before overwriting existing password unless forced.
passrs edit pass-name [--echo,-e]
    Insert a new password or edit an existing password.
passrs generate [--echo,-e] [--no-symbols,-n] [--force,-f] pass-name [pass-length]
    Generate a new password of pass-length (or 32 if unspecified) with optionally no symbols.
    Prompt before overwriting existing password unless forced.
    Optionally, echo the password back to the console.
passrs rm [--recursive,-r] [--force,-f] pass-name
    Remove existing password or directory, optionally forcefully
passrs mv [--force,-f] old-path new-path
    Renames or moves old-path to new-path, optionally forcefully, re-encrypting as it goes.
passrs cp [--force,-f] old-path new-path
    Copies old-path to new-path, optionally forcefully, re-encrypting as it goes.
passrs git git-command-args...
    If the password store is a git repository, execute a git command
    specified by git-command-args.
passrs help
    Show this help text.
passrs version
    Show version information.
```

## TODO

### Commands

- [x] Create storage ( `init [subfolder]` )
- [x] Show storage ( `[ls] [subfolder]` )
- [x] Find matching names ( `find pass-names...` )
  - [ ] Cleanup printing to account for no further entries that match
- [x] Show password ( `[show] pass-name` )
  - [x] Clipboard support ( `[--clip,-c]` )
  - [x] QRCode generation ( `[--qrcode,-q]` )
- [ ] Grep all plaintext passwords ( `grep [GREPOPTIONS] search-string` )
- [x] Insert password ( `insert pass-name` )
  - [x] Show password ( `[--echo,-e]` )
  - [x] Force overwrite ( `[--force,-f]` )
  - [ ] Multiline support ( `[--echo,-e | --multiline,-m]` )
    - [ ] Clip line numbers ( `--clip[=line-number],-c[=line-number]` )
- [x] Edit password ( `edit pass-name` )
- [x] Generate password ( `generate,gen` )
  - [x] Insert into storage ( `[pass-name]` )
  - [x] No special characters ( `[--no-symbols,-n]` )
  - [x] Clipboard support ( `[--clip,-c]` )
  - [ ] Inline support? ( `[--in-place,-i]` )
  - [x] Force overwrite ( `[--force,-f]` )
  - [x] Custom length ( `[--length,-l]` )
- [x] Delete password/path ( `rm pass-name` )
  - [x] Recursive ( `[--recursive,-r]` )
  - [x] Force overwrite ( `[--force,-f]` )
- [x] Move password ( `mv [--force,-f] old-path new-path` )
- [x] Copy password ( `cp [--force,-f] old-path new-path` )
- [ ] Git support ( `git git-command-args...` )
- [x] Show help ( `help` )
- [x] Show version ( `version` )

### Refactoring

- [ ] Cleaner argument parsing
- [ ] Better usage on command parse error
- [ ] Unit tests
- [x] Libify `passrs` with separate `bin` compilation
